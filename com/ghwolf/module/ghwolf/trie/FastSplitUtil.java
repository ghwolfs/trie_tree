package com.ghwolf.module.ghwolf.trie;

/**
 * 快速分割字符串工具，本类提供基于字符进行字符串分割并转换为字符串数组的方法，
 * 此操作性能高于String.split，StringTokenizer，apache的StringUtils.split。
 * <p>
 * String.split使用的是正则进行匹配，性能较差，但是高于apache的StringUtils.split。
 * </p><p>
 * StringTokenizer性能高于String.split，但是存在一个问题，他对于"a.b..c."这种字符串无法解析中间和结尾空白的内容，
 * 也就是说对他来说"a.b..c."这种字符串的解析结果和"a.b.c"是一样的。这会造成可能的逻辑错误。
 * </p><p>
 * apache的StringUtils.split存在一个严重的问题就是使用了List进行字符串存储而后toArray，这会造成很严重的性能损耗。
 * </p><p>
 * 而Spring得StringUtils.split只能解析第一个，所以不满足业务需求。
 * </p>
 * <p>
 * 本类弥补了以上各种分割操作的缺点，基于目前最直接简单的需求，在通过字符进行字符串分割时，提供了最为快速得操作。
 * </p>
 * <p>
 * 注意：DK9之后，由于字符串不在以char[]存储，而是byte[]存储，对于此操作的性能是否有影响并未测试。
 * </p>
 * 
 * @author Ghwolf
 * @since 2020年1月2日 下午8:42:36
 * @since JDK8
 * @version 1.0
 */
public class FastSplitUtil {

	/**
	 * 快速根据字符分割一个字符串，并转换为字符串数组.
	 * 
	 * @param str 要分割得字符串
	 * @param c 分割字符
	 * @return 返回分割后得字符串，如果字符串为null，则返回长度为0得字符串数组。否则长度至少为1
	 */
	public static final String[] fastSplit(String str, final char c) {
		if (str == null) return new String[0];
		String[] arr = new String[countChar(str, c) + 1];
		int i = 0, j, k = 0;
		while ((j = str.indexOf(c, i)) != -1) {
			arr[k] = str.substring(i, j);
			i = ++ j;
			k ++;
		}
		arr[k] = str.substring(i);
		return arr;
	}
	/**
	 * 统计一个字符在一个字符串中出现的次数.
	 * 
	 * @param str 要让统计的字符串
	 * @param c 要查询的字符
	 * @return 返回出现的次数，没有返回0
	 * @throws NullPointerException 如果str为null
	 */
	public static final int countChar(final String str, final char c) {
		int count = 0, x = 0;
		final int length = str.length();
		for (; x < length; x ++) {
			if (str.charAt(x) == c) count ++;
		}
		return count;
	}

}
